# Purge firewalld
# TODO: Make this work for more than centos
class lysnetwork::iptables::purge_firewalld {
  if $facts['os']['name'] == 'CentOS' and $facts['os']['release']['major'] == 7 {
    service { 'firewalld':
      ensure => stopped,
      enable => false,
    }
    ~> exec { 'really, really, kill firewalld, with fire.':
      name => 'systemctl stop firewalld ; systemctl mask firewalld',
    }
    package { 'firewalld':
      ensure => purged,
    }
  }
}

