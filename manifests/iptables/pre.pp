# Default rules
class lysnetwork::iptables::pre {
  # Firewall {
  #   require => undef,
  # }

  firewall { '000 accept all icmp':
    proto  => 'icmp',
    jump => 'accept',
  }
  -> firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    jump  => 'accept',
  }
  -> firewall { '002 accept all from Lysator':
    proto  => 'all',
    source => '130.236.254.0/24',
    jump => 'accept',
  }
  -> firewall { '003 accept all from Lysator':
    proto  => 'all',
    source => '130.236.253.0/24',
    jump => 'accept',
  }
  -> firewall { '004 accept all from Lysator':
    proto  => 'all',
    source => '192.168.0.0/16',
    jump => 'accept',
  }
  -> firewall { '005 accept all from Lysator':
    proto  => 'all',
    source => '172.16.0.0/12',
    jump => 'accept',
  }
  -> firewall { '006 accept all from Lysator':
    proto  => 'all',
    source => '10.0.0.0/9',
    jump => 'accept',
  }
  -> firewall { '007 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    jump => 'accept',
  }

  # IPv6 config

  firewall { '000 accept all icmp IPv6':
    proto    => 'ipv6-icmp',
    jump   => 'accept',
    protocol => 'ip6tables',
  }
  -> firewall { '001 accept all from Lysator IPv6':
    proto    => 'all',
    source   => '2001:6b0:17:f0a0::0/64',
    jump   => 'accept',
    protocol => 'ip6tables',
  }
  -> firewall { '002 accept related established rules IPv6':
    proto    => 'all',
    state    => ['RELATED', 'ESTABLISHED'],
    jump   => 'accept',
    protocol => 'ip6tables',
  }
}

