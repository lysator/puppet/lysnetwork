  # Purge firewalld
  # TODO: Make this work for more than centos
# Standard setup for Lysators ip tables.
# Purges firewalld, and should delete any routes not managed by
# puppet.
#
# This version deafults to allow. For a strict version, see:
# ::lysnetwork::iptables_strict
class lysnetwork::iptables {
  require ::lysnetwork::iptables::purge_firewalld
  require ::firewall

  include ::lysnetwork::iptables::pre

  firewallchain { [
    'f2b-sshd:filter:IPv4',
    'f2b-sshd:filter:IPv6',
    'f2b-sshlongterm:filter:IPv4',
    'f2b-sshlongterm:filter:IPv6',
    ]:
    purge  => false,
  }

  firewallchain { ['INPUT:filter:IPv4', 'INPUT:filter:IPv6']:
    purge  => true,
    ignore => [
      'f2b-ssh', # ignore the fail2ban jump rule
    ],
  }

  Firewall {
    require    => Class['lysnetwork::iptables::pre'],
    pkg_ensure => present,
    purge      => false,
  }
}
