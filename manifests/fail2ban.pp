class lysnetwork::fail2ban (
  $config = undef,
){
  if $facts['os']['name'] == 'CentOS' {
    require ::epel
  }
  package { 'fail2ban':
    ensure => latest,
    before => File['/etc/fail2ban/jail.local'],
  }

  service { 'fail2ban':
    ensure  => running,
    enable  => true,
    require => [ Package['fail2ban'],
    File['/etc/fail2ban/jail.local'],
    File['/etc/fail2ban/action.d/iptables-common.conf'],],
  }

  file { '/etc/fail2ban/jail.local':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => hash2ini($config, {quote_char => ''}),
    notify  => Service['fail2ban'],
  }

  file {
    '/etc/fail2ban/action.d/iptables-common.conf':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0644',
      source => 'puppet:///modules/lysnetwork/fail2ban/actions_iptable_common',
      notify => Service['fail2ban'],
  }
}
