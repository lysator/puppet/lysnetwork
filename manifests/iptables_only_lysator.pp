class lysnetwork::iptables_only_lysator {
  require ::lysnetwork::iptables::purge_firewalld
  require ::firewall

  firewallchain { ['INPUT:filter:IPv4', 'INPUT:filter:IPv6']:
    purge  => true,
    policy => drop,
    ignore => [
      'f2b-ssh', # ignore the fail2ban jump rule
    ],
  }

  firewallchain { [
    'f2b-sshd:filter:IPv4',
    'f2b-sshd:filter:IPv6',
    'f2b-sshlongterm:filter:IPv4',
    'f2b-sshlongterm:filter:IPv6',
    ]:
    purge  => false,
  }

  # IPv4
  firewall { '001 accept all on lo':
    proto   => all,
    iniface => 'lo',
    jump  => 'accept',
  }

  firewall { '002 accept related and established':
    proto  => all,
    state  => ['RELATED', 'ESTABLISHED',],
    jump => 'accept',
  }

  firewall { '003 allow lysators ipv4 network':
    proto    => all,
    source   => '130.236.254.0/24',
    jump   => 'accept',
    protocol => 'iptables'
  }

  # IPv6

  firewall { '001 accept all on lo ipv6':
    proto    => all,
    iniface  => 'lo',
    jump   => 'accept',
    protocol => 'ip6tables',
  }

  firewall { '002 accept related and established ipv6':
    proto    => all,
    state    => ['RELATED', 'ESTABLISHED',],
    jump   => 'accept',
    protocol => 'ip6tables',
  }

  firewall { '003 allow lysators ipv6 network':
    proto    => all,
    source   => '2001:6b0:17:f0a0::/64',
    jump   => 'accept',
    protocol => 'ip6tables'
  }
}

