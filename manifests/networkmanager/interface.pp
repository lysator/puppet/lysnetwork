# @summary Minimal class for managing a network interface using NetworkManager
#
# Note that no effort is made to clean up any previous state.
#
# We currently don't rewrite the whole file, as we'd have to generate and maintain a uuid in the connection section of the file.
define lysnetwork::networkmanager::interface (
  Stdlib::IP::Address::V4::CIDR            $ipv4_address,
  Stdlib::IP::Address::Nosubnet            $ipv4_gateway,
  Array[Stdlib::IP::Address::Nosubnet]     $ipv4_dns_servers,
  String                                   $ipv4_dns_search,
  Stdlib::IP::Address::V6::CIDR            $ipv6_address,
  Stdlib::IP::Address::V6::Nosubnet        $ipv6_gateway,
  Array[Stdlib::IP::Address::V6::Nosubnet] $ipv6_dns_servers,
  String                                   $ipv6_dns_search,
) {
  $interface = $title

  $ipv4_dns_servers_string = $ipv4_dns_servers.map |$s| { "${s};" }.join('')
  $ipv6_dns_servers_string = $ipv6_dns_servers.map |$s| { "${s};" }.join('')

  augeas { "networkmanager interface ${name}":
    lens    => 'Desktop.lns',  # .nmconnection files use the same file format as .desktop files (GLib "key files")
    incl    => "/etc/NetworkManager/system-connections/${interface}.nmconnection",
    changes => [
      'set ipv4/method manual',
      "set ipv4/address1 ${ipv4_address}",
      "set ipv4/gateway ${ipv4_gateway}",
      "set ipv4/dns ${ipv4_dns_servers_string}",
      "set ipv4/dns-search ${ipv4_dns_search}",

      'set ipv6/method manual',
      "set ipv6/address1 ${ipv6_address}",
      "set ipv6/gateway ${ipv6_gateway}",
      "set ipv6/dns ${ipv6_dns_servers_string}",
      "set ipv6/dns-search ${ipv6_dns_search}",
    ],
  }
  ~> exec { '/usr/bin/nmcli connection reload':
    refreshonly => true,
  }
  -> exec { "/usr/bin/nmcli device reapply ${interface}":
    refreshonly => true,
    subscribe   => Augeas["networkmanager interface ${name}"],
  }

  # Clean up the network-scripts config files (if they exist) to prevent confusion
  file { "/etc/sysconfig/network-scripts/ifcfg-${interface}":
    ensure  => absent,
    require => Augeas["networkmanager interface ${name}"],
  }
}
