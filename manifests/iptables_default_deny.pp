class lysnetwork::iptables_default_deny {
  require ::lysnetwork::iptables::purge_firewalld
  require ::firewall

  firewallchain { ['INPUT:filter:IPv4', 'INPUT:filter:IPv6']:
    purge  => true,
    policy => drop,
    ignore => [
      'f2b-ssh', # ignore the fail2ban jump rule
    ],
  }

  firewallchain { [
    'f2b-sshd:filter:IPv4',
    'f2b-sshd:filter:IPv6',
    'f2b-sshlongterm:filter:IPv4',
    'f2b-sshlongterm:filter:IPv6',
    ]:
    purge  => false,
  }


  # IPV4

  firewall { '000 accept all icmp':
    proto  => icmp,
    jump => 'accept',
  }
  -> firewall { '001 accept all on lo':
    proto   => all,
    iniface => 'lo',
    jump  => 'accept',
  }
  -> firewall { '002 accept related and established':
    proto  => all,
    state  => ['RELATED', 'ESTABLISHED',],
    jump => 'accept',
  }

  # IPV6

  firewall { '000 accept all icmp ipv6':
    proto    => 'ipv6-icmp',
    jump   => 'accept',
    protocol => 'ip6tables',
  }
  -> firewall { '001 accept all on lo ipv6':
    proto    => all,
    iniface  => 'lo',
    jump   => 'accept',
    protocol => 'ip6tables',
  }
  -> firewall { '002 accept related and established ipv6':
    proto    => all,
    state    => ['RELATED', 'ESTABLISHED',],
    jump   => 'accept',
    protocol => 'ip6tables',
  }

  # Allow ssh
  firewall { '922 allow ssh':
    proto  => tcp,
    dport  => '22',
    jump => 'accept',
  }

  firewall { '922 allow ssh ipv6':
    proto    => tcp,
    dport    => '22',
    jump   => 'accept',
    protocol => 'ip6tables',
  }
}
