class lysnetwork::hosts (
  Boolean $mgmt = false,
) {
  concat { '/etc/hosts':
    ensure => present,
  }

  concat::fragment { '/etc/hosts/00-own':
    target  => '/etc/hosts',
    content => "${::ipaddress} ${::fqdn} ${::hostname}\n";
  }

  concat::fragment { '/etc/hosts/01-defaults':
    target => '/etc/hosts',
    source => 'puppet:///modules/lysnetwork/default-hosts';
  }

  if $mgmt {
    concat::fragment { '/etc/hosts/02-mgmt':
      target => '/etc/hosts',
      source => 'puppet:///modules/lysnetwork/mgmt-hosts';
    }
  }
}
