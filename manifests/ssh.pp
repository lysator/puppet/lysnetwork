#This class manages ssh-clients and servers.
class lysnetwork::ssh(
  $server_package = undef,
  $client_package = undef,
  $service_name = undef
) {

  ensure_packages([$server_package, $client_package])

  service { $service_name:
    ensure    => running,
    enable    => true,
    subscribe => [
      Package[$server_package],
      File['/etc/ssh/sshd_config'],
    ],
  }


  # Generate host keys if they are missing.
  (['rsa', 'ecdsa']).each |$type| {
    exec { "ssh-keygen -t ${type} -f /etc/ssh/ssh_host_${type}_key":
      creates => "/etc/ssh/ssh_host_${type}_key",
      path    => ['/bin', '/usr/bin'],
    }
  }


  file { '/etc/ssh/sshd_config':
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
    source => ["puppet:///modules/lysnetwork/ssh/sshd_config-${facts['os']['name']}-${facts['os']['release']['major']}",
               "puppet:///modules/lysnetwork/ssh/sshd_config-${facts['os']['name']}"],
  }

  file {
    '/etc/ssh/ssh_config':
      ensure => file,
      owner  => 'root',
      group  => 'root',
      mode   => '0444',
      source => "puppet:///modules/lysnetwork/ssh/ssh_config-${facts['os']['name']}",
  }

  file { '/etc/ssh/sshd_config.d':
    ensure  => directory,
    owner   => root,
    source  => 'puppet:///modules/lysnetwork/ssh/sshd_config.d/',
    recurse => remote,
  }

  file { '/etc/ssh/ssh_config.d':
    ensure  => directory,
    owner   => root,
    source  => 'puppet:///modules/lysnetwork/ssh/ssh_config.d/',
    recurse => remote,
  }
}
