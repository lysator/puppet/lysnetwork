class lysnetwork::public_ip (
  $nm_controlled = undef,
  Boolean $manage_resolvconf = true,
){
  $iface = $facts['networking']['primary']
  $last_octet = Integer(split($facts['networking']['ip'], '\.')[3],10)
  $last_octet_16 = String($last_octet, '%x')

  if fact('os.name') == 'Fedora' and versioncmp(fact('os.release.major'), '35') >= 0 {
    ::lysnetwork::networkmanager::interface { $iface:
      ipv4_address     => "${fact('networking.ip')}/24",
      ipv4_gateway     => '130.236.254.1',
      ipv4_dns_servers => ['130.236.254.4', '130.236.254.225'],
      ipv4_dns_search  => 'lysator.liu.se',
      ipv6_address     => "2001:6b0:17:f0a0::${last_octet_16}/64",
      ipv6_gateway     => '2001:6b0:17:f0a0::1',
      ipv6_dns_servers => ['2001:6b0:17:f0a0::4', '2001:6b0:17:f0a0::e1'],
      ipv6_dns_search  => 'lysator.liu.se',
    }
    # disable systemd-resolved
    service { 'systemd-resolved':
      ensure => stopped,
      enable => false,
    }
    -> exec { '/usr/bin/rm -f /etc/resolv.conf':
      onlyif => '/usr/bin/test -h /etc/resolv.conf',
    }
    # avoid managing the service by using an exec resource
    ~> exec { '/usr/bin/systemctl restart NetworkManager':
      refreshonly => true,
    }
  } elsif versioncmp($facts['systemd_version'], '215') >= 0 {
    ::lysnetwork::systemd_networkd::interface { $iface:
      addresses => [
        "${facts['networking']['ip']}/24",
        "2001:6b0:17:f0a0::${last_octet_16}/64",
      ],
      gateways  => [
        '130.236.254.1',
        '2001:6b0:17:f0a0::1',
      ],
    }

    if $manage_resolvconf {
      file { '/etc/resolv.conf':
        content => epp('lysnetwork/resolv.conf.epp', {
          search     => 'lysator.liu.se',
          nameserver => [
            '130.236.254.4',
            '130.236.254.225',
            '2001:6b0:17:f0a0::4',
            '2001:6b0:17:f0a0::e1',
          ],
        }),
      }
    }
  } else {
    fail("Networking not implemented for ${facts['os']['family']}")
  }
}
