class lysnetwork::systemd_networkd::base {
    case $facts['os']['family'] {
      'RedHat': {
        ensure_packages(['systemd-networkd'])
      }
      'Debian': {

        package { 'resolvconf': ensure => absent }
        service { 'networking':
          enable => false,
        }

      }
      default: {}
    }

    service { 'systemd-networkd':
      enable => true,
    }

    file { '/etc/systemd/network':
      ensure    => directory,
    }
}
