define lysnetwork::systemd_networkd::interface (
  String $interface = $name,
  Array[String] $addresses = [],
  Array[String] $gateways = [],
){
  include lysnetwork::systemd_networkd::base

  $content = {
    'Match'   => { 'Name' => $interface },
    'Network' => {
      'Address'      => $addresses,
      'Gateway'      => $gateways,
      'IPv6AcceptRA' => '0',
    },
  }

  file { "/etc/systemd/network/${interface}.network":
    ensure  => present,
    content => epp('lysnetwork/unit_file.epp', { data => $content }),
    notify  => Service['systemd-networkd'],
  }
}
